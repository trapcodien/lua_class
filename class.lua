--[[ 
		makeClass function in Lua.
		Author: Trapcodien
		Email : trapcodien@hotmail.fr
--]]


local concat_table = function(...) 
	local t3 = {} 
	for _,t in ipairs(table.pack(...)) do 
		for k,v in pairs(t) do t3[k] = v end
	end
	return t3
end

function makeClass(prototype, trait, ...)

	local creator = {}
	local parents = table.pack(...)
	creator.prototypes = {}
	creator.traits = trait
	
	local prev_trait = creator.traits
	for i,_ in ipairs(parents) do
		table.insert(creator.prototypes, parents[i].prototypes)
		setmetatable(prev_trait, { __index = parents[i].traits, __newindex = parents[i].traits })
		prev_trait = parents[i].traits
	end
	table.insert(creator.prototypes, prototype)
	creator.prototypes = concat_table(table.unpack(creator.prototypes))

	setmetatable(creator, { __index = trait, __newindex = trait, __call = function (self, ...) return self.new(...) end })
	function creator.new(...)
		local self = concat_table(creator.prototypes)
		if creator.traits.__init then creator.traits.__init(self, ...) end
		setmetatable(self, { __index = trait, __newindex = trait, __tostring = trait.__tostring })
		return self
	end
	return creator
end

